%global _empty_manifest_terminate_build 0
%global sum Python library for non-blocking use of Jabber/XMPP

Name:		python-nbxmpp
Version:	4.0.1
Release:	1
Summary:	%{sum}
License:	GPL-3.0-only
URL:		https://dev.gajim.org/gajim/python-nbxmpp/
Source0:	https://files.pythonhosted.org/packages/52/5e/61d1edcd20b367fc54804c7f80a5608f6155f8068d144f5b6817316ac0e9/nbxmpp-4.0.1.tar.gz
BuildArch:	noarch

Requires:	python3-precis-i18n
Requires:	python3-packaging
Requires:	python3-idna

%global desc python-nbxmpp is a Python library that provides a way for Python applications\
to use Jabber/XMPP networks in a non-blocking way.\
\
Features:\
- Asynchronous\
- ANONYMOUS, EXTERNAL, GSSAPI, SCRAM-SHA-1, DIGEST-MD5, PLAIN, and\
    X-MESSENGER-OAUTH2 authentication mechanisms.\
- Connection via proxies\
- TLS\
- BOSH (XEP-0124)\
- Stream Management (XEP-0198)

%description
%{desc}

%package -n python3-nbxmpp
Summary:	%{sum}
Recommends:     python3-kerberos
Provides:	python-nbxmpp = %{version}-%{release}
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
Requires:       python3-gobject >= 3.42.0
Requires:       glib2 >= 2.60
Requires:       libsoup3
%description -n python3-nbxmpp
%{desc}

%package help
Summary:	Development documents and examples for nbxmpp
Provides:	python3-nbxmpp-doc
%description help
%{desc}

%prep
%autosetup -n nbxmpp-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-nbxmpp -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Fri Aug 23 2024 yueyaoqiang <yueyaoqiang@kylinos.cn> - 4.0.1-1
- update to 4.0.1

* Mon Aug 19 2024 yueyaoqiang <yueyaoqiang@kylinos.cn> - 4.0.0-1
- update to 4.0.0

* Tue Nov 22 2022 wangjunqi <wangjunqi@kylinos.cn> - 3.2.5-1
- Update package to version 3.2.5

* Fri Jul 26 2019 Fedora Release Engineering <releng@fedoraproject.org> - 0.6.10-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_31_Mass_Rebuild

* Thu Apr 25 2019 Michal Schmidt <mschmidt@redhat.com> - 0.6.10-1
- Upstream release 0.6.10.

* Sat Feb 02 2019 Fedora Release Engineering <releng@fedoraproject.org> - 0.6.9-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_30_Mass_Rebuild

* Thu Jan 24 2019 Michal Schmidt <mschmidt@redhat.com> - 0.6.9-1
- Upstream release 0.6.9.

* Wed Nov 14 2018 Michal Schmidt <mschmidt@redhat.com> - 0.6.8-1
- Upstream release 0.6.8.

* Thu Oct 11 2018 Miro Hrončok <mhroncok@redhat.com> - 0.6.6-4
- Python2 binary package has been removed
  See https://fedoraproject.org/wiki/Changes/Mass_Python_2_Package_Removal

* Sat Jul 14 2018 Fedora Release Engineering <releng@fedoraproject.org> - 0.6.6-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Tue Jun 19 2018 Miro Hrončok <mhroncok@redhat.com> - 0.6.6-2
- Rebuilt for Python 3.7

* Mon May 21 2018 Michal Schmidt <mschmidt@redhat.com> - 0.6.6-1
- Upstream release 0.6.6.

* Mon Mar 19 2018 Michal Schmidt <mschmidt@redhat.com> - 0.6.4-1
- Upstream release 0.6.4.

* Wed Mar 07 2018 Michal Schmidt <mschmidt@redhat.com> - 0.6.3-1
- Upstream release 0.6.3.

* Fri Feb 09 2018 Fedora Release Engineering <releng@fedoraproject.org> - 0.6.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Mon Jan 15 2018 Michal Schmidt <mschmidt@redhat.com> - 0.6.2-1
- Upstream release 0.6.2.

* Mon Dec 04 2017 Michal Schmidt <mschmidt@redhat.com> - 0.6.1-1
- Upstream release 0.6.1.

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.5.6-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Thu Jun 08 2017 Michal Schmidt <mschmidt@redhat.com> - 0.5.6-1
- Upstream release 0.5.6.

* Mon Feb 06 2017 Michal Schmidt <mschmidt@redhat.com> - 0.5.5-1
- Upstream release 0.5.5.
- New upstream location and tarball format.
- Refer to python2-kerberos using its actual package name.

* Mon Dec 19 2016 Miro Hrončok <mhroncok@redhat.com> - 0.5.3-5
- Rebuild for Python 3.6

* Tue Jul 19 2016 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.3-4
- https://fedoraproject.org/wiki/Changes/Automatic_Provides_for_Python_RPM_Packages

* Thu Feb 18 2016 Michal Schmidt <mschmidt@redhat.com> - 0.5.3-3
- Build as both python2-nbxmpp and python3-nbxmpp. (#1309621)

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.5.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Mon Jul 27 2015 Matej Cepl <mcepl@redhat.com> - 0.5.3-1
- Upstream release 0.5.3.

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.5.2-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Mon Mar 02 2015 Michal Schmidt <mschmidt@redhat.com> - 0.5.2-1
- Upstream bugfix release 0.5.2.

* Thu Oct 16 2014 Michal Schmidt <mschmidt@redhat.com> - 0.5.1-1
- New upstream release, required by Gajim 0.16.

* Mon Aug 11 2014 Michal Schmidt <mschmidt@redhat.com> - 0.5-1
- New upstream release.

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Wed Mar 19 2014 Michal Schmidt <mschmidt@redhat.com> - 0.4-1
- Initial Fedora packaging.
